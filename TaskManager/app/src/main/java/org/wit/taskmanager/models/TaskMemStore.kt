package org.wit.taskmanager.models

import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

var lastId = 0L

internal fun getId(): Long {
  return lastId++
}

class TaskMemStore : TaskStore, AnkoLogger {

  val tasks = ArrayList<TaskModel>()

  override fun findAll(): List<TaskModel> {
    return tasks
  }

  override fun create(task: TaskModel) {
    task.id = getId()
    tasks.add(task)
    logAll()
  }

  override fun update(task: TaskModel) {
    var foundTask: TaskModel? = tasks.find { p -> p.id == task.id }
    if (foundTask != null) {
      foundTask.title = task.title
      foundTask.description = task.description
      foundTask.selection = task.selection
      foundTask.image = task.image
      foundTask.lat = task.lat
      foundTask.lng = task.lng
      foundTask.zoom = task.zoom
      logAll();
    }
  }

  override fun delete(task: TaskModel) {
    tasks.remove(task)
  }

  fun logAll() {
    tasks.forEach { info("${it}") }
  }
}