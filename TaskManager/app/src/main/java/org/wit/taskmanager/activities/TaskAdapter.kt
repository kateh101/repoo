package org.wit.taskmanager.activities

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.card_task.view.*
import org.wit.taskmanager.R
import org.wit.taskmanager.helpers.readImageFromPath
import org.wit.taskmanager.models.TaskModel

interface TaskListener {
  fun onTaskClick(task: TaskModel)
}

class TaskAdapter constructor(private var tasks: List<TaskModel>,
                              private val listener: TaskListener) : RecyclerView.Adapter<TaskAdapter.MainHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainHolder {
    return MainHolder(LayoutInflater.from(parent?.context).inflate(R.layout.card_task, parent, false))
  }

  override fun onBindViewHolder(holder: MainHolder, position: Int) {
    val task = tasks[holder.adapterPosition]
    holder.bind(task, listener)
  }

  override fun getItemCount(): Int = tasks.size

  class MainHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(task: TaskModel, listener: TaskListener) {
      itemView.taskTitle.text = task.title
      itemView.description.text = task.description
      itemView.TaskSelection.text = task.selection
      itemView.imageIcon.setImageBitmap(readImageFromPath(itemView.context, task.image))
      itemView.setOnClickListener { listener.onTaskClick(task) }
    }
  }
}