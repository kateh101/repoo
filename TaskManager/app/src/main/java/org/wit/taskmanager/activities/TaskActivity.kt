package org.wit.taskmanager.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_task.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import org.wit.taskmanager.R
import org.wit.taskmanager.helpers.readImage
import org.wit.taskmanager.helpers.readImageFromPath
import org.wit.taskmanager.helpers.showImagePicker
import org.wit.taskmanager.main.MainApp
import org.wit.taskmanager.models.Location
import org.wit.taskmanager.models.TaskModel

class TaskActivity : AppCompatActivity(), AnkoLogger {

  var task = TaskModel()
  lateinit var app: MainApp
  val IMAGE_REQUEST = 1

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_task)
    //to create variable to hold spinner
    var manager = arrayOf<String>("To be completed","In Progress","Complete")
    var TaskSelection = findViewById(R.id.TaskSelection) as Spinner
    var adapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,manager)
   TaskSelection.adapter = adapter
    //Listener
    TaskSelection.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
      override fun onItemSelected(adapterView:AdapterView<*>,view: View, i: Int, l: Long)
      {
        Toast.makeText(this@TaskActivity, manager[i], Toast.LENGTH_SHORT).show()      }

    override fun onNothingSelected(adapterView: AdapterView<*>){

    }}
    toolbarAdd.title = title
    setSupportActionBar(toolbarAdd)
    info("Task Activity started..")

    app = application as MainApp
    var edit = false

    if (intent.hasExtra("task_edit")) {
      edit = true
      task = intent.extras.getParcelable<TaskModel>("task_edit")
      taskTitle.setText(task.title)
      description.setText(task.description)
      taskImage.setImageBitmap(readImageFromPath(this, task.image))
      if (task.image != null) {
        chooseImage.setText(R.string.change_task_image)
      }
      btnAdd.setText(R.string.save_task)
    }

    btnAdd.setOnClickListener() {
      task.title = taskTitle.text.toString()
      task.description = description.text.toString()
      if (task.title.isEmpty()) {
        toast(R.string.enter_task_title)
      } else {
        if (edit) {
          app.tasks.update(task.copy())
        } else {
          app.tasks.create(task.copy())
        }
      }
      info("add Button Pressed: $taskTitle")
      setResult(AppCompatActivity.RESULT_OK)
      finish()
    }
    btnAdd2.setOnClickListener() {
      task.title = taskTitle.text.toString()
      task.description = description.text.toString()
      if (task.title.isEmpty()) {
        toast(R.string.enter_task_title)
      } else {
        if (edit) {
          app.tasks.delete(task.copy())
        }
      }
      info("add Button Pressed: $taskTitle")
      setResult(AppCompatActivity.RESULT_OK)
      finish()
    }
    chooseImage.setOnClickListener {
      showImagePicker(this, IMAGE_REQUEST)
    }
  }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
      menuInflater.inflate(R.menu.menu_task, menu)
      return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
      when (item?.itemId) {
        R.id.item_cancel -> {
          finish()
        }
      }
      return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
      super.onActivityResult(requestCode, resultCode, data)
      when (requestCode) {
        IMAGE_REQUEST -> {
          if (data != null) {
            task.image = data.getData().toString()
            taskImage.setImageBitmap(readImage(this, resultCode, data))
            chooseImage.setText(R.string.change_task_image)
          }
        }

      }
    }
  }