package org.wit.taskmanager.main

import android.app.Application
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.wit.taskmanager.models.TaskJSONStore
import org.wit.taskmanager.models.TaskStore

class MainApp : Application(), AnkoLogger {

  lateinit var tasks: TaskStore

  override fun onCreate() {
    super.onCreate()
    tasks = TaskJSONStore(applicationContext)
    info("Task started")
  }
}