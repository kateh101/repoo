package org.wit.taskmanager.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import kotlinx.android.synthetic.main.activity_task_list.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivityForResult
import org.wit.taskmanager.R
import org.wit.taskmanager.main.MainApp
import org.wit.taskmanager.models.TaskModel

class TaskListActivity : AppCompatActivity(), TaskListener {

  lateinit var app: MainApp

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_task_list)
    app = application as MainApp
    toolbarMain.title = title
    setSupportActionBar(toolbarMain)

    val layoutManager = LinearLayoutManager(this)
    recyclerView.layoutManager = layoutManager
    recyclerView.adapter = TaskAdapter(app.tasks.findAll(), this)
    loadTasks()
  }

  private fun loadTasks() {
    showTasks( app.tasks.findAll())
  }

  fun showTasks (tasks: List<TaskModel>) {
    recyclerView.adapter = TaskAdapter(tasks, this)
    recyclerView.adapter?.notifyDataSetChanged()
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.menu_main, menu)
    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    when (item?.itemId) {
      R.id.item_add -> startActivityForResult<TaskActivity>(0)
    }
    return super.onOptionsItemSelected(item)
  }

  override fun onTaskClick(task: TaskModel) {
    startActivityForResult(intentFor<TaskActivity>().putExtra("task_edit", task), 0)
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    loadTasks()
    super.onActivityResult(requestCode, resultCode, data)
  }
}