package org.wit.taskmanager.models

interface TaskStore {
  fun findAll(): List<TaskModel>
  fun create(task: TaskModel)
  fun update(task: TaskModel)
  fun delete(task: TaskModel)
}