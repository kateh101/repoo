package org.wit.taskmanager.models

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import org.jetbrains.anko.AnkoLogger
import org.wit.taskmanager.helpers.*
import java.util.*

val JSON_FILE = "tasks.json"
val gsonBuilder = GsonBuilder().setPrettyPrinting().create()
val listType = object : TypeToken<java.util.ArrayList<TaskModel>>() {}.type

fun generateRandomId(): Long {
  return Random().nextLong()
}

class TaskJSONStore : TaskStore, AnkoLogger {

  val context: Context
  var tasks = mutableListOf<TaskModel>()

  constructor (context: Context) {
    this.context = context
    if (exists(context, JSON_FILE)) {
      deserialize()
    }
  }

  override fun findAll(): MutableList<TaskModel> {
    return tasks
  }

  override fun create(task: TaskModel) {
    task.id = generateRandomId()
    tasks.add(task)
    serialize()
  }
  
  override fun update(task: TaskModel) {
    var foundTask: TaskModel? = tasks.find { p -> p.id == task.id }
    if (foundTask != null) {
      foundTask.title = task.title
      foundTask.description = task.description
      foundTask.image = task.image
      foundTask.lat = task.lat
      foundTask.lng = task.lng
      foundTask.zoom = task.zoom
    }
    serialize()
  }

  override fun delete(task: TaskModel) {
    tasks.remove(task)
    serialize()
  }

  private fun serialize() {
    val jsonString = gsonBuilder.toJson(tasks, listType)
    write(context, JSON_FILE, jsonString)
  }

  private fun deserialize() {
    val jsonString = read(context, JSON_FILE)
    tasks = Gson().fromJson(jsonString, listType)
  }
}
